<?php

use yii\db\Migration;

/**
 * Filling tables with default data
 */
class m200909_091658_fill_tables extends Migration
{
    const BRAND_TABLE_NAME = 'brand';
    const ENGINE_TYPE_TABLE_NAME = 'engine_type';
    const DRIVE_UNIT_TABLE_NAME = 'drive_unit';
    const CAR_MODEL_TABLE_NAME = 'car_model';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(self::BRAND_TABLE_NAME, ['name' => 'toyota']);
        $this->insert(self::BRAND_TABLE_NAME, ['name' => 'lexus']);
        $this->insert(self::BRAND_TABLE_NAME, ['name' => 'audi']);
        $this->insert(self::BRAND_TABLE_NAME, ['name' => 'mercedes-benz']);
        $this->insert(self::BRAND_TABLE_NAME, ['name' => 'Volkswagen']);

        $this->insert(self::DRIVE_UNIT_TABLE_NAME, ['name' => 'four-wheel']);
        $this->insert(self::DRIVE_UNIT_TABLE_NAME, ['name' => 'front-wheel']);
        $this->insert(self::DRIVE_UNIT_TABLE_NAME, ['name' => 'rear-wheel']);

        $this->insert(self::ENGINE_TYPE_TABLE_NAME, ['name' => 'gasoline']);
        $this->insert(self::ENGINE_TYPE_TABLE_NAME, ['name' => 'diesel']);
        $this->insert(self::ENGINE_TYPE_TABLE_NAME, ['name' => 'hybrid']);

        $this->insertCar('prius', 'toyota', 'four-wheel', 'hybrid');
        $this->insertCar('land cruiser', 'toyota', 'four-wheel', 'gasoline');
        $this->insertCar('camry', 'toyota', 'front-wheel', 'gasoline');
        $this->insertCar('corolla', 'toyota', 'front-wheel', 'diesel');
        $this->insertCar('rav4', 'toyota', 'four-wheel', 'gasoline');
        $this->insertCar('CT200 h', 'lexus', 'front-wheel', 'hybrid');
        $this->insertCar('IS300 h', 'lexus', 'rear-wheel', 'hybrid');
        $this->insertCar('ES300 h', 'lexus', 'front-wheel', 'hybrid');
        $this->insertCar('LS600 h', 'lexus', 'four-wheel', 'hybrid');
        $this->insertCar('RX450 h', 'lexus', 'four-wheel', 'hybrid');
        $this->insertCar('A6', 'audi', 'front-wheel', 'hybrid');
        $this->insertCar('Q5', 'audi', 'four-wheel', 'hybrid');
        $this->insertCar('Q7', 'audi', 'four-wheel', 'gasoline');
        $this->insertCar('A4', 'audi', 'front-wheel', 'diesel');
        $this->insertCar('A8', 'audi', 'four-wheel', 'gasoline');
        $this->insertCar('S400', 'mercedes-benz', 'rear-wheel', 'hybrid');
        $this->insertCar('C 63 AMG', 'mercedes-benz', 'rear-wheel', 'gasoline');
        $this->insertCar('A200', 'mercedes-benz', 'front-wheel', 'diesel');
        $this->insertCar('C180', 'mercedes-benz', 'front-wheel', 'diesel');
        $this->insertCar('E200', 'mercedes-benz', 'rear-wheel', 'gasoline');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete(self::CAR_MODEL_TABLE_NAME);
        $this->delete(self::ENGINE_TYPE_TABLE_NAME);
        $this->delete(self::DRIVE_UNIT_TABLE_NAME);
        $this->delete(self::BRAND_TABLE_NAME);
    }

    /**
     * Функция для добавления модели машины
     *
     * @param string $name
     * @param string $brand
     * @param string $driveUnit
     * @param string $engineType
     */
    private function insertCar(string $name, string $brand, string $driveUnit, string $engineType): void
    {
        $this->execute(
            "INSERT INTO car_model
            (name, brand_id, drive_unit_id, engine_type_id)
            VALUES(
                '$name',
                (SELECT id FROM brand WHERE name='$brand'),
                (SELECT id FROM drive_unit WHERE name='$driveUnit'),
                (SELECT id FROM engine_type WHERE name='$engineType')
            )"
        );
    }
}
