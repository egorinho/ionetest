<?php

use yii\db\Migration;

/**
 * Handles the creation of table the model of car.
 */
class m200909_085012_create_car_model_table extends Migration
{
    const TABLE_NAME = 'car_model';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string('255')->notNull(),
            'brand_id' => $this->integer()->notNull(),
            'engine_type_id' => $this->integer()->notNull(),
            'drive_unit_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'fk-car_model-brand_id',
            self::TABLE_NAME,
            'brand_id',
            'brand',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-car_model-engine_type_id',
            self::TABLE_NAME,
            'engine_type_id',
            'engine_type',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-car_model-drive_unit_id',
            self::TABLE_NAME,
            'drive_unit_id',
            'drive_unit',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
