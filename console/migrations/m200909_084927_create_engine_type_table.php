<?php

use yii\db\Migration;

/**
 * Handles the creation of table engine_type.
 */
class m200909_084927_create_engine_type_table extends Migration
{
    const TABLE_NAME = 'engine_type';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string('255')->notNull()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
