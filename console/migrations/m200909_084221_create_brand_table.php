<?php

use yii\db\Migration;

/**
 * Handles the creation of table brand.
 */
class m200909_084221_create_brand_table extends Migration
{
    const TABLE_NAME = 'brand';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string('255')->notNull()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
