<?php

use yii\db\Migration;

/**
 * Handles the creation of table drive_unit.
 */
class m200909_084938_create_drive_unit_table extends Migration
{
    const TABLE_NAME = 'drive_unit';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string('255')->notNull()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
