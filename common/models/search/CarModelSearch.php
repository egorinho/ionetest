<?php

namespace common\models\search;

use common\models\Brand;
use common\models\DriveUnit;
use common\models\EngineType;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * CarModelSearch represents the model behind the search form of `common\models\CarModel`.
 */
class CarModelSearch extends Model
{
    /** @var string $brand название марки машины */
    public $brand;

    /** @var string $engineType тип двигателя */
    public $engineType;

    /** @var string $driveUnit тип привода */
    public $driveUnit;

    /** @var string $name название модели машины */
    public $name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand', 'engineType', 'driveUnit'], 'trim'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = new Query();
        $query->from('car_model');
        $query->select('car_model.name as name, brand.name as brand, engine_type.name as engineType, drive_unit.name as driveUnit');
        $query->leftJoin(Brand::tableName(), 'brand.id = car_model.brand_id');
        $query->leftJoin(EngineType::tableName(), 'engine_type.id = car_model.engine_type_id');
        $query->leftJoin(DriveUnit::tableName(), 'drive_unit.id = car_model.drive_unit_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params, '');

        if (!$this->validate()) {
            return $dataProvider;
        }

        // если выбираем любой бренд то очищаем поле для выбора модели авто
        if (!$this->brand && $this->name) {
            $this->name = null;
        }

        $query->andFilterWhere(['brand.name' => $this->brand]);
        $query->andFilterWhere(['engine_type.name' => $this->engineType]);
        $query->andFilterWhere(['drive_unit.name' => $this->driveUnit]);
        $query->andFilterWhere(['like', 'car_model.name', $this->name]);

        return $dataProvider;
    }
}
