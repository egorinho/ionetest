<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "car_model".
 *
 * @property int $id
 * @property string $name
 * @property int $brand_id
 * @property int $engine_type_id
 * @property int $drive_unit_id
 *
 * @property Brand $brand
 * @property DriveUnit $driveUnit
 * @property EngineType $engineType
 */
class CarModel extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'brand_id', 'engine_type_id', 'drive_unit_id'], 'required'],
            [['brand_id', 'engine_type_id', 'drive_unit_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::class, 'targetAttribute' => ['brand_id' => 'id']],
            [['drive_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => DriveUnit::class, 'targetAttribute' => ['drive_unit_id' => 'id']],
            [['engine_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => EngineType::class, 'targetAttribute' => ['engine_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'brand_id' => 'Brand ID',
            'engine_type_id' => 'Engine Type ID',
            'drive_unit_id' => 'Drive Unit ID',
        ];
    }

    /**
     * Gets query for Brand.
     *
     * @return ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::class, ['id' => 'brand_id']);
    }

    /**
     * Gets query for DriveUnit.
     *
     * @return ActiveQuery
     */
    public function getDriveUnit()
    {
        return $this->hasOne(DriveUnit::class, ['id' => 'drive_unit_id']);
    }

    /**
     * Gets query for EngineType.
     *
     * @return ActiveQuery
     */
    public function getEngineType()
    {
        return $this->hasOne(EngineType::class, ['id' => 'engine_type_id']);
    }
}
