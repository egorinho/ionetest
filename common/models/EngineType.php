<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "engine_type".
 *
 * @property int $id
 * @property string $name
 *
 * @property CarModel[] $carModels
 */
class EngineType extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'engine_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for CarModels.
     *
     * @return ActiveQuery
     */
    public function getCarModels()
    {
        return $this->hasMany(CarModel::class, ['engine_type_id' => 'id']);
    }
}
