<?php

namespace common\repositories;

use yii\db\Query;

/**
 * Репозиторий для поиска моделей авто
 * @package common\repositories
 */
class CarModelRepository
{
    /**
     * Получить массив ['name' => 'name'] вида, где name - название модели авто
     *
     * @param string $brandName название марки авто
     * @return array
     */
    public function findNamesByBrandName(string $brandName): array
    {
        $query = new Query();
        return $query->from('brand')
            ->select('car_model.name as name')
            ->leftJoin('car_model', 'car_model.brand_id = brand.id')
            ->where("brand.name = '$brandName'")
            ->indexBy('car_model.name')
            ->column();
    }
}