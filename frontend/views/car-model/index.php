<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\web\View;
use common\models\search\CarModelSearch;
use yii\data\ActiveDataProvider;
use common\models\Brand;
use common\models\EngineType;
use common\models\DriveUnit;
use yii\widgets\Pjax;

/* @var $this View */
/* @var $searchModel CarModelSearch */
/* @var $dataProvider ActiveDataProvider */
/* @var $title string */
/* @var $carModels array */

$this->title = $title;
?>
<div class="car-model-index">
    <?php Pjax::begin(); ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'name',
                'filter' =>  Html::activeDropDownList(
                $searchModel,
                'name',
                $carModels,
                ['class'=>'form-control', 'prompt' => 'Select brand', 'name' => 'name', 'disabled' => !$searchModel->brand]
            )
            ],
            [
                'attribute' => 'brand',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'brand',
                    Brand::find()->select('name')->indexBy('name')->asArray()->column(),
                    ['class'=>'form-control', 'prompt' => 'Select brand', 'name' => 'brand', 'onchange' => "$('#carmodelsearch-name').val(null)"]
                ),
            ],
            [
                'attribute' => 'engineType',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'engineType',
                    EngineType::find()->select('name')->indexBy('name')->asArray()->column(),
                    ['class'=>'form-control', 'prompt' => 'Select engine type', 'name' => 'engineType']
                ),
            ],
            [
                'attribute' => 'driveUnit',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'driveUnit',
                    DriveUnit::find()->select('name')->indexBy('name')->asArray()->column(),
                    ['class'=>'form-control', 'prompt' => 'Select engine type', 'name' => 'driveUnit']
                ),
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
