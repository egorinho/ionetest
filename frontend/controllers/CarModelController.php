<?php

namespace frontend\controllers;

use common\repositories\CarModelRepository;
use Yii;
use common\models\search\CarModelSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * CarModelController implements the CRUD actions for CarModel model.
 */
class CarModelController extends Controller
{
    private $carModelRepository;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->carModelRepository = Yii::createObject(CarModelRepository::class);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CarModel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $title = $this->getTitle($searchModel);
        $carModels = $this->carModelRepository->findNamesByBrandName($searchModel->brand);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $title,
            'carModels' => $carModels,
        ]);
    }

    /**
     * Возвращает нужный title
     *
     * @param CarModelSearch $carModelSearch
     * @return string
     */
    private function getTitle(CarModelSearch $carModelSearch): string
    {
        if ($carModelSearch->brand && $carModelSearch->name) {
            return "Продажа новых автомобилей $carModelSearch->brand $carModelSearch->name в Санкт-Петербурге";
        }

        if ($carModelSearch->brand) {
            return "Продажа новых автомобилей $carModelSearch->brand в Санкт-Петербурге";
        }

        return 'Продажа новых автомобилей в Санкт-Петербурге';
    }
}
